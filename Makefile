# executable file
MAIN=tbasic

SOURCES=tbasic.c

# Compiler
CC=clang
CFLAGS=-Wall -Werror -Wshadow -g -DDEBUG

# header files other than /usr/include
#INCLUDES=-Iinclude

# library paths in addition to /usr/lib
#LFLAGS=-Llib

# libraries to link into executable:
#LIBS=-lmylib

# C object files
# This uses Suffix Replacement within a macro:
#   $(name:string1=string2)
OBJECTS=$(SOURCES:.c=.o)

all:	$(MAIN)
	@echo built $(MAIN)

$(MAIN): $(OBJECTS)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJECTS) $(LFLAGS) $(LIBS)

# this is a suffix replacement rule for building .o's from .c's
# $<: prerequisite of the rule (.c)
# $@: target of the rule (.o)
.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@

clean:
	$(RM) *.o $(MAIN)
