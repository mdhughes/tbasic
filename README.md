# tbasic
###### Copyright ©2018,2019 by Mark Damon Hughes. All Rights Reserved.
###### BSD 3-Clause License, see LICENSE

**tbasic** is a small, fast implementation of Integer Tiny BASIC, suitable for shell scripting, running old program listings (with minor adaptation), and writing terminal-oriented utilities and games.

Running `tbasic FILENAME` will load and run FILENAME, and exit when it executes `end`.

A tbasic program can be run as a shell script:

```
#!/usr/bin/env tbasic
10 print "Hello, world!"
20 end
```

There is no interactive editor/runner, likely never will be: It's meant as a script or programmed language. You can write short programs without a script from your shell:

```
tbasic -e '10 print "Hello, shell!"'
```

Text encoding is 8-bit, but not Unicode aware; emoji and such should pass through, but that depends on your shell and locale. All words not in strings are internally converted to lowercase; you can use UPPERCASE for historical authenticity if you like.

----
### Language

- [x] LINE {STATEMENT}: Each program line must have a number at the beginning, the "address" of that line, from 1 to 32767. Program lines cannot exceed 256 characters. Multiple statements per line are not supported. You should number your programs in steps of 5 or 10, with steps of 1 for conditional branches.
- [x] \#COMMENT: Comments start with `#` and continue to the end of the line. Single-quote `'` like classic BASIC is also allowed, but discouraged, since it won't highlight correctly in many editors. The `rem` statement is not used.
- [x] NUMBER: Numbers are integers only (up to 32-bit).
- [x] "STRING": Strings contain any series of non-" characters, of unlimited length. There are no escape codes, use `chr$()` and see [ASCII](#ascii-character-set) below.
- [x] VARIABLE: Variable names can be up to 8 chars long, can be any word not reserved as a statement, and are case-insensitive; `foo` and `FOO` are the same. Variables ending $ are string variables.
- [x] VALUE: NUMBER, STRING, or VARIABLE.
- [x] EXPR: Expressions are algebraic, standard operator precedence applies. Unless a line says EXPR, it only takes VALUE.

### Statements

#### Control

- [x] end {EXPR}: Ends program execution. If EXPR is given, it is used as the return code to the shell; 0 indicates success, other numbers indicate errors.
- [ ] for VAR=START to END {step STEP}: Starts a new loop on VAR.
- [ ] gosub LINE: Jumps to LINE, remembers next line for `return`.
- [x] goto LINE: Jumps to LINE, does not remember where it's been.
- [x] if EXPR {then LINENUM} {else LINENUM}: Computes an expression. If true (non-zero), jumps to `then` line, if false (zero), jumps to `else` line. There are no conditional statements, use blocks of lines and `goto`:

	```
	100 if n=1 then 101 else 105
	101 print n;" ";widget$
	102 goto 110
	105 print n;" ";widget$;"s"
	110 #...
	```

- [ ] next VAR: Completes `for` loop, closes any more recent loops.
- [ ] return: Returns to line after `gosub` which called this subroutine. Also closes all `for` loops created since that gosub.
- [ ] wait EXPR: Sleeps for EXPR milliseconds; `wait N*1000` will wait for N seconds.

#### Memory

- [ ] data CONST {, CONST}...: Define data values, does nothing until `read`.
- [ ] dim VAR[SIZE] {, VAR[SIZE]}...: Dimensions an array. Note: All arrays are 1-dimensional, and array size and access use [square brackets], not (parentheses).
- [x] let VAR=VALUE { OP VALUE }...: Assigns result of an expression to a variable. The `let` keyword is optional but recommended.
- [ ] read VAR {, VAR}...: Pulls the next item(s) from `data` and assigns them to VAR(s).
- [ ] restore LINE: Sets the next line `read` will search from for `data` statements.

#### I/O

- [x] error {VALUE|,|;}...: Prints just like `print`, but to stderr. Use `goto` or `end` to handle fatal errors after printing a message.
- [ ] inkey VAR: Reads a single character from stdin and assigns the character code to a variable, or 0 if stdin is currently empty; it does not wait for the user.
- [ ] input {STRING,} VAR {, VAR}...: If given an initial STRING, displays that as prompt, otherwise no prompt. Reads a line from stdin and assigns it to variables. Each comma matches a comma surrounded by any amount of spaces in the input, and the remainder is put in the last variable. So `input a$,b$,c$` given "hello, guy, what it is" sets a$ to "hello", b$ to "guy", c$ to "what it is". Numeric variables get the value converted by the `val()` function.
- [x] print {VALUE|,|;}...: Prints each value to stdout. `,` prints a horizontal tab, `;` separates other values with no space. If the last token is VALUE, a newline is added. Note these cannot be EXPR, do all work in `let` and display variables.

#### Graphics

- [ ] graphics W, H: Opens a graphics window with size W, H. Size 0, 0 will close the window.
- [ ] cls: Clear the graphics screen, with the current color as background.
- [ ] color R, G, B: Sets the pen color for future drawing.
- [ ] plot X, Y: Draws a pixel at X, Y
- [ ] drawto X, Y: Draws a line from last point to X, Y
- [ ] fill X, Y: floodfill from X, Y

----
### Operators

- [x] +: Add when used on numbers, concatenate when used on strings. Mixed types produce a string.
- [x] -, *, /, %: Subtract, Multiply, Divide, Modulo. Only works on numbers.
- [x] =, <>, <, <=, >, >=: Equals, Not Equals, Less Than, Less Than or Equal, Greater Than, Greater Than or Equal. Comparisons work on numbers or strings, but not on mixed types.

----
### Functions

- [ ] abs(A): Absolute value.
- [ ] and(A, B), not (A), or (A, B): Logical And, Or, Not. 0 is false, any other number is true. Only works on numbers.
- [ ] argc(): Number of command-line arguments.
- [ ] band(A, B), bnot (A), bor (A, B), bshift(A, B), bxor(A, B): Binary And, Not, Or, Shift, Xor. Shift B positive is left, B negative is right. Only works on numbers. Operations beyond 32-bit are undefined.
- [ ] rnd(A): Random number from 0 to A, if A is 1 or higher; if A is 0, a random number from 0 to 2^31-1 (highest positive integer).

#### Math Functions

3-digit fixed point is used, so all values are multiplied by 1000. Angles are given in radians. I may add `deg` and `rad` statements to switch between degrees/radians.

```
10 let pi=3142
15 let x=cos(pi)
20 print x # -1000
```

- [ ] atn(A): Arctangent
- [ ] cos(A): Cosine
- [ ] exp(A): Natural exponent
- [ ] log(A): Natural logarithm
- [ ] pow(A, B): Raise A to B power
- [ ] sin(A): Sine
- [ ] sqr(A): Square root
- [ ] tan(A): Tangent

#### String Functions

- [ ] argv$(A): Command-line argument.
- [ ] env$(A$): Command-line environment value.
- [ ] asc(A$): ASCII value of first character in A$. The character set is undefined above 127.
- [ ] chr$(A): 1-character string with ASCII character A. The character set is undefined above 127.
- [ ] instr(A$, B$, C): Returns the first appearance of B$ in A$, starting at index C. 1 is the first character, returns 0 if B$ is not found. Case-sensitive only.
- [ ] left$(A$, B): Left-most B characters of A$, right-padded with spaces if B > len(A).
- [ ] len(A$): Length of A$.
- [ ] lower$(A$): Returns lowercase of A$
- [ ] mid$(A$, B, C): Characters of A$ from positions B through C, inclusive. 1 is the first character, 0 is invalid. Negatives count from end of string.
- [ ] right$(A$, B): Right-most B characters of A$, left-padded with spaces if B > len(A).
- [ ] str$(A {, B}): Returns string version of number A. If B is given, it is divided by B first in floating point, so str$(3142, 1000) returns "3.142".
- [ ] upper$(A$): Returns uppercase of A$
- [ ] val(A$ {, B}): Integer value of A$. If B is given, A is first multiplied by B in floating point, so val("3.142", 1000) returns 3142.

#### Unsupported

Some common BASIC statments not in **tbasic**:

- [ ] rem: Use `#` or `'` instead.
- [ ] `?`: use `print` instead. May be added later.
- [ ] `:` No multiple statements per line. Insert new lines after the current line, so `10 A=1:PRINT A` becomes:
	<br>`10 a=1`
	<br>`11 print a`
- [ ] catalog: No interactive mode, so no purpose.
- [ ] clear: same
- [ ] cont: same
- [ ] kill: same
- [ ] list: same
- [ ] load: same
- [ ] new: same
- [ ] save: same
- [ ] stop: same
- [ ] on EXPR goto LINE {, LINE}...: Use `if a=1 then 100` and so on.
- [ ] on EXPR gosub LINE {, LINE}...: same
- [ ] system EXPR: Executing shell commands, with user confirmation or a command-line flag for safety, is likely to be added later.

----
### ASCII Character Set

```
  0 nul    1 soh    2 stx    3 etx    4 eot    5 enq    6 ack    7 bel
  8 bs     9 ht    10 nl    11 vt    12 np    13 cr    14 so    15 si
 16 dle   17 dc1   18 dc2   19 dc3   20 dc4   21 nak   22 syn   23 etb
 24 can   25 em    26 sub   27 esc   28 fs    29 gs    30 rs    31 us
 32 sp    33  !    34  "    35  #    36  $    37  %    38  &    39  '
 40  (    41  )    42  *    43  +    44  ,    45  -    46  .    47  /
 48  0    49  1    50  2    51  3    52  4    53  5    54  6    55  7
 56  8    57  9    58  :    59  ;    60  <    61  =    62  >    63  ?
 64  @    65  A    66  B    67  C    68  D    69  E    70  F    71  G
 72  H    73  I    74  J    75  K    76  L    77  M    78  N    79  O
 80  P    81  Q    82  R    83  S    84  T    85  U    86  V    87  W
 88  X    89  Y    90  Z    91  [    92  \    93  ]    94  ^    95  _
 96  `    97  a    98  b    99  c   100  d   101  e   102  f   103  g
104  h   105  i   106  j   107  k   108  l   109  m   110  n   111  o
112  p   113  q   114  r   115  s   116  t   117  u   118  v   119  w
120  x   121  y   122  z   123  {   124  |   125  }   126  ~   127 del
```

(from BSD `man ascii`)


###### EOF

<style type="text/css">
ul {
	margin-top: 0;
	margin-bottom: 0;
}
li > p {
	margin-top: 0;
	margin-bottom: 0;
}
</style>
