#! ./tbasic -d

1 #test1.bas
5 print "READY"
6 error "WARNING: This is a language test script, not a useful example."

7 let n$="Mark"
8 print "Hello, "; n$;"!"

10 let y=2019
11 print "The year is "; y; "!"

20 let a=-1
21 print "Negative 1: "; a

30 let b=3+4*9/(1+2)+-5 # end-of-line comment
31 print "Ten: "; b

50 if a<b then 60 else 70

60 print "Sixty"
61 goto 100

70 print "Wrong: Seventy"
100 print "End"
110 end 69
