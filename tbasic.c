/* tbasic.c
 * TinyBasic
 * Created 2018-12-31
 * Copyright © 2018,2019 Mark Damon Hughes. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "tbasic.h"

#pragma mark - BASIC Program Types

typedef enum {
	STMT_Unknown = 0,
	STMT_Let,
	STMT_Print,
	STMT_Error,
	STMT_If,
	STMT_Then,
	STMT_Else,
	STMT_Goto,
	STMT_End,
	StatementCount,
} StatementType;

static const char *STATEMENT_NAMES[] = {
	NULL,
	"let",
	"print",
	"error",
	"if",
	"then",
	"else",
	"goto",
	"end",
};

typedef enum {
	SYM_StackStart = 1, // internal flag
	SYM_Remark = '#',
	SYM_String = '"',
	SYM_Number = '0',
	SYM_Statement = 's',
	SYM_Variable = 'v',
	SYM_NEQ = ('<'<<8)|'>',
	SYM_LTE = ('<'<<8)|'=',
	SYM_GTE = ('>'<<8)|'=',
	SYM_EQ = '=',
	SYM_LT = '<',
	SYM_GT = '>',
	SYM_Plus = '+',
	SYM_Minus = '-',
	SYM_Times = '*',
	SYM_Divide = '/',
	SYM_Modulo = '%',
	SYM_UnaryMinus = '_',
	SYM_Semicolon = ';',
	SYM_Comma = ',',
	SYM_Paren = '(',
	SYM_ParenClose = ')',
} SymbolType;

int operatorPrecedence(SymbolType type) {
	switch (type) {
		case SYM_NEQ:
		case SYM_LTE:
		case SYM_GTE:
		case SYM_EQ:
		case SYM_LT:
		case SYM_GT:
			return 6;
		case SYM_Plus:
		case SYM_Minus:
			return 4;
		case SYM_Times:
		case SYM_Divide:
		case SYM_Modulo:
			return 3;
		case SYM_UnaryMinus:
			return 2;
		default:
			return 0;
	}
}

typedef struct {
	SymbolType type;
	int number;
	char *string;
	void *next;
} Symbol;

Symbol *makeSymbol(SymbolType type, int number, char *string) {
	Symbol *sym = (Symbol *)malloc( sizeof(Symbol) );
	sym->type = type;
	sym->number = number;
	sym->string = string;
	sym->next = NULL;
	return sym;
}

void freeSymbol(Symbol *sym) {
	if (sym->string) {
		free(sym->string);
	}
	if (sym->next) {
		freeSymbol(sym->next);
	}
	free(sym);
}

void fprintSymbol(FILE *fp, Symbol *sym, char *suffix) {
	if ( ! fp) {
		fp = stdout;
	}
	if ( ! sym) {
		fprintf(fp, "{NULL} %s", suffix);
		fflush(fp);
		return;
	}
	switch ( (int)sym->type) {
		case SYM_StackStart:
			fprintf(fp, "{STACK} ");
			break;
		case SYM_Remark:
			fprintf(fp, "#%s", sym->string);
			break;
		case SYM_Statement:
			fprintf(fp, "%s ", STATEMENT_NAMES[sym->number]);
			break;
		case SYM_Variable:
			fprintf(fp, "%s ", sym->string);
			break;
		case SYM_String:
			fprintf(fp, "\"%s\" ", sym->string);
			break;
		case SYM_Number:
			fprintf(fp, "%d ", sym->number);
			break;
		case SYM_NEQ:
			fprintf(fp, "<> ");
			break;
		case SYM_LTE:
			fprintf(fp, "<= ");
			break;
		case SYM_GTE:
			fprintf(fp, ">= ");
			break;
		default:
			fprintf(fp, "%c ", sym->type);
			break;
	}

	if (sym->next) {
		fprintSymbol(fp, sym->next, suffix);
	} else {
		fputs(suffix, fp);
	}
	fflush(fp);
}

typedef struct {
	char *name;
	int number;
	char *string;
} Variable;

BOOL isStringVarname(char *name) {
	return name[strlen(name)-1] == '$';
}

Variable *makeVariable(char *name) {
	Variable *var = (Variable *)malloc( sizeof(Variable) );
	var->name = name;
	var->number = 0;
	var->string = isStringVarname(name) ? "" : NULL;
	return var;
}

void freeVariable(Variable *var) {
	// don't free name, since that's in your source somewhere.
	free(var);
}

static BOOL basicDebug;
static char *basicFilename;
static int lineNumber;

/** Highest allowed BASIC line number. */
#define BASIC_LINENUM_MAX 32768

/** Sets line number to 1…BASIC_LINENUM_MAX, returns EX_CONTINUE if OK, EX_TERMINAL if out of range. */
int gotoLineNumber(int n) {
	if (n >= 1 && n < BASIC_LINENUM_MAX) {
		lineNumber = n;
		return EX_CONTINUE;
	} else {
		basicError("ERROR: Invalid line number ");
		fprintf(stderr, "%d\n", n);
		return EX_TERMINAL;
	}
}

/** Advances line number, returns EX_CONTINUE if OK, EX_TERMINAL if out of range. */
int gotoNextLineNumber() {
	if (lineNumber >= 0 && lineNumber < BASIC_LINENUM_MAX-1) {
		++lineNumber;
		return EX_CONTINUE;
	} else {
		return EX_TERMINAL;
	}
}

static Symbol *program[BASIC_LINENUM_MAX];
// TODO: index of next statement for faster sequential search

/** Number of variables allowed. */
#define BASIC_VAR_MAX 256

/** Variable name length allowed. */
#define BASIC_VARNAME_MAX 8

static Variable *variable[BASIC_VAR_MAX];

/** Looks up variable `name`, returns existing or new variable. */
Variable *getVariable(char *name) {
	// TODO: qsort variable list, use binary search
	if ( ! name) {
		basicError("NULL variable name!\n");
		return NULL;
	}
	if (strlen(name) == 0 || strlen(name) > BASIC_VARNAME_MAX) {
		basicError("ERROR: Invalid variable name ");
		fprintf(stderr, "'%s'\n", name);
		return NULL;
	}
	Variable *var;
	int firstEmpty = -1;
	for (int i = 0; i < BASIC_VAR_MAX; ++i) {
		var = variable[i];
		if ( ! var) {
			if (firstEmpty < 0) {
				firstEmpty = i;
			}
		} else if (strcmp(var->name, name) == 0) {
			return var;
		}
	}
	if (firstEmpty < 0) {
		basicError("ERROR: Out of variable space");
		return NULL;
	}
	var = makeVariable(name);
	variable[firstEmpty] = var;
	return var;
}

/** Debug tool, displays a single variable. */
void fprintVariable(FILE *fp, Variable *var) {
	if (isStringVarname(var->name)) {
		fprintf(fp, "    %s=\"%s\"\n", var->name, var->string ?: "null");
	} else {
		fprintf(fp, "    %s=%d\n", var->name, var->number);
	}
}

/** Debug tool, displays all variables. */
void fprintVariables(FILE *fp) {
	for (int i = 0; i < BASIC_VAR_MAX; ++i) {
		if (variable[i]) {
			fprintVariable(fp, variable[i]);
		}
	}
}

#pragma mark - String & Linebuffer

/** Maximum BASIC line input length. */
#define BASIC_LINE_LEN 256

static char linebuf[BASIC_LINE_LEN];
static int linebufIndex;

/** Returns a copy of `s` from `start` to `end` indices, inclusive. */
char *substring(char *s, int start, int end) {
	int len = strlen(s);
	if (start >= len) {
		return "";
	}
	if (start < 0) {
		start = 0;
	}
	if (end < start) {
		end = start;
	}
	if (end >= len) {
		end = len-1;
	}
	int count = end-start+1; // Should this be +1?
	if (count <= 0) {
		return "";
	}
	char *cp = (char *)malloc(count);
	memcpy(cp, s+start, count);
	cp[count] = '\0';
	return cp;
}

/** Returns a new string containing `a`+`b`. */
char *stringAppend(char *a, char *b) {
	int alen = strlen(a), blen = strlen(b);
	char *cp = (char *)malloc(alen + blen + 1);
	memcpy(cp, a, alen);
	memcpy(cp+alen, b, blen);
	cp[alen+blen] = '\0';
	return cp;
}

/** Returns a new string containing a copy of `a`. */
char *stringCopy(char *a) {
	int alen = strlen(a);
	char *cp = (char *)malloc(alen + 1);
	memcpy(cp, a, alen);
	cp[alen] = '\0';
	return cp;
}

/** Returns a new string containing `n` printed out. */
char *stringFromNumber(int n) {
	static char sfnbuffer[32];
	snprintf(sfnbuffer, 32, "%d", n);
	return stringCopy(sfnbuffer);
}

/** Converts `s` to lowercase. */
char *strToLower(char *s) {
	for (int i = 0, len = strlen(s); i < len; ++i) {
		s[i] = tolower(s[i]);
	}
	return s;
}

/** Removes all spaces from end of `s`. */
char *strTrimRight(char *s) {
	char *end = s + strlen(s) - 1;
	while(end > s && isspace(*end)) {
		end[0] = '\0';
		--end;
	}
	return s;
}

char linebufPeek() {
	return linebuf[linebufIndex];
}

void linebufReset() {
	linebufIndex = 0;
	memset(linebuf, '\0', BASIC_LINE_LEN);
}

BOOL isLinebufEOL() {
	return ! linebuf[linebufIndex] || linebuf[linebufIndex] == '\n';
}

BOOL isLinebufTerminated(char terminator) {
	char c = linebuf[linebufIndex];
	return c == '\0' || c == '\n' || (terminator == ' ' ? isspace(c) : c == terminator);
}

#pragma mark - BASIC Parser

Symbol *nextSymbol() {
	int start = linebufIndex;

	while ( ! isLinebufEOL() && isspace(linebufPeek())) { // advance to non-space
		++linebufIndex;
		++start;
	}
	if (isLinebufEOL()) {
		return NULL;
	}
	char c = linebufPeek();
	switch (c) {
		case '#': case '\'': { // remark
			++start;
			do {
				++linebufIndex;
			} while ( ! isLinebufEOL());
			char *string = strTrimRight(substring(linebuf, start, linebufIndex-1));
			return makeSymbol(SYM_Remark, 0, string);
		}
		case '"': { // string
			++start;
			do {
				++linebufIndex;
			} while ( ! isLinebufTerminated('\"'));
			if (linebufPeek() == '\"') {
				++linebufIndex;
			}
			char *string = substring(linebuf, start, linebufIndex-2);
			return makeSymbol(SYM_String, 0, string);
		}
		case '<': case '>': {
			++linebufIndex;
			char next = linebufPeek();
			if (c == '<' && next == '>') {
				++linebufIndex;
				return makeSymbol(SYM_NEQ, 0, NULL);
			} else if (next == '=') {
				++linebufIndex;
				return makeSymbol(c == '<' ? SYM_LTE : SYM_GTE, 0, NULL);
			} else {
				return makeSymbol(c == '<' ? SYM_LT : SYM_GT, 0, NULL);
			}
			break;
		}
		case ';': case ',':
		case '(': case ')':
		case '=': case '+': case '-': case '*': case '/': case '%':
		case '_': {
			++linebufIndex;
			return makeSymbol(c, 0, NULL);
		}
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9': {
			do {
				++linebufIndex;
			} while ( ! isLinebufEOL() && isdigit(linebufPeek()) );
			char *string = strTrimRight(substring(linebuf, start, linebufIndex));
			return makeSymbol(SYM_Number, atoi(string), NULL);
		}
		default: {
			if (isalpha(c)) {
				do {
					++linebufIndex;
				} while ( ! isLinebufEOL() && (isalnum(linebufPeek()) || linebufPeek() == '$') );
				char *string = strToLower(strTrimRight(substring(linebuf, start, linebufIndex-1)));
				for (int i = 1; i < StatementCount; ++i) {
					if (strcmp(STATEMENT_NAMES[i], string) == 0) {
						return makeSymbol(SYM_Statement, i, NULL);
					}
				}
				return makeSymbol(SYM_Variable, 0, string);
			} else {
				basicError("SYNTAX: Invalid char: ");
				fprintf(stderr, "'%c'\n", c);
			}
		}
	}
	return NULL;
}

int parseLine() {
	int rc = gotoNextLineNumber();
	if (rc != EX_CONTINUE) {
		return rc;
	}
	strTrimRight(linebuf);
	// if (basicDebug) {
	// 	fprintf(stderr, "Parsing %s\n", linebuf);
	// }
	Symbol *sym = nextSymbol();
	if ( ! sym) {
		return EX_CONTINUE;
	}
	if (sym->type == SYM_Remark) {
		if (basicDebug) {
			fprintf(stderr, "-");
			fprintSymbol(stderr, sym, "\n");
		}
		freeSymbol(sym);
		return EX_CONTINUE;
	}
	if (sym->type != SYM_Number) {
		basicError("SYNTAX: Expected line number, but got: ");
		fprintSymbol(stderr, sym, "\n");
		return EX_USAGE;
	}
	if (sym->number <= 0 || sym->number >= BASIC_LINENUM_MAX) {
		basicError("SYNTAX: Line number out of range 1..");
		fprintf(stderr, "%d: %d\n", BASIC_LINENUM_MAX, sym->number);
		return EX_USAGE;
	}
	// store line in `program`
	int pc = sym->number;
	program[pc] = sym;
	if (basicDebug) {
		fprintf(stderr, "+%05d ", pc);
	}
	Symbol *parent = sym;

	for (;;) {
		sym = nextSymbol();
		if ( ! sym) {
			break;
		}
		if (sym->type == SYM_Remark) {
			if (basicDebug) {
				fprintSymbol(stderr, sym, "");
			}
			// unbound remark, throw it away
			freeSymbol(sym);
			break;
		}
		if (basicDebug) {
			fprintSymbol(stderr, sym, "");
		}
		parent->next = sym;
		parent = sym;
	}
	if (basicDebug) {
		fprintf(stderr, "\n");
	}
	return EX_CONTINUE;
}

/** Loads a basic program `filename` into memory. Returns error code or EXIT_SUCCESS. */
int basicLoad(char *filename) {
	// if (basicDebug) {
	// 	fprintf(stderr, "load %s\n", filename);
	// }
	basicFilename = filename;
	FILE *fp = fopen(filename, "r");
	if ( ! fp || ferror(fp)) {
		basicError("ERROR: Unable to open file");
		return EX_NOINPUT;
	}
	int rc = EXIT_SUCCESS;
	linebufReset();
	while (fgets(linebuf, BASIC_LINE_LEN, fp)) {
		rc = parseLine();
		if (rc != EX_CONTINUE) {
			break;
		}
		linebufReset();
	}
	if (ferror(fp)) {
		basicError("ERROR: Unable to read file");
		rc = errno;
	}
	fclose(fp);
	return EXIT_SUCCESS;
}

#pragma mark - BASIC Runtime

/** Returns current Symbol and advances symPtr if not at end. Returns NULL at end. */
Symbol *advanceSymbol(Symbol **symPtr) {
	Symbol *sym = *symPtr;
	if (sym) {
		*symPtr = sym->next;
	}
	return sym;
}

/** Returns current Symbol and advances symPtr if `type` matches, otherwise does nothing & returns NULL. */
Symbol *advanceSymbolIfType(Symbol **symPtr, SymbolType type) {
	if (*symPtr && (*symPtr)->type == type) {
		Symbol *sym = *symPtr;
		*symPtr = sym->next;
		return sym;
	} else {
		return NULL;
	}
}

/** Returns current Symbol and advances symPtr if `type` and `number` match, otherwise does nothing & returns NULL. */
Symbol *advanceSymbolIfTypeAndNumber(Symbol **symPtr, SymbolType type, int number) {
	if (*symPtr && (*symPtr)->type == type && (*symPtr)->number == number) {
		Symbol *sym = *symPtr;
		*symPtr = sym->next;
		return sym;
	} else {
		return NULL;
	}
}

BOOL symbolStackEmpty(Symbol *stackHead) {
	return stackHead->next ? NO : YES;
}

/** Returns final node in Symbol chain starting at `stackHead`. */
Symbol *symbolStackPeek(Symbol *stackHead) {
	Symbol *ptr = stackHead;
	while (ptr->next) {
		ptr = ptr->next;
	}
	return ptr;
}

/** Unlinks and returns final node in Symbol chain starting at `stackHead`. Returns NULL if you try to pop head! */
Symbol *symbolStackPop(Symbol *stackHead) {
	Symbol *prev = stackHead;
	Symbol *ptr = stackHead;
	while (ptr->next) {
		prev = ptr;
		ptr = ptr->next;
	}
	prev->next = NULL;
	return ptr;
}

/** Pushes `sym` unmodified, should only be used for new symbols! */
void symbolStackPushOriginal(Symbol *stackHead, Symbol *sym) {
	symbolStackPeek(stackHead)->next = sym;
}

/** Pushes a copy of `sym`, without `->next` pointer. */
void symbolStackPushCopy(Symbol *stackHead, Symbol *sym) {
	Symbol *newSym = makeSymbol(sym->type, sym->number, sym->string);
	symbolStackPeek(stackHead)->next = newSym;
}

void basicExprReorder(Symbol *sym, Symbol *workStack, Symbol *exprStack) {
	// if (basicDebug) {
	// 	basicError("Algebraic: ");
	// 	fprintSymbol(stderr, sym, "\n");
	// }

	BOOL lastValue = NO;

	while (sym) {
		if (sym->type == SYM_Number || sym->type == SYM_Variable || sym->type == SYM_String) {
			symbolStackPushCopy(exprStack, sym);
			lastValue = YES;
		} else if (sym->type == SYM_Paren) {
			symbolStackPushCopy(workStack, sym);
			lastValue = NO;
		} else if (sym->type == SYM_ParenClose) {
			while ( ! symbolStackEmpty(workStack) && symbolStackPeek(workStack)->type != SYM_Paren) {
				Symbol *top = symbolStackPop(workStack);
				symbolStackPushOriginal(exprStack, top);
			}
			freeSymbol( symbolStackPop(workStack) );
			lastValue = YES;
		} else if (sym->type == SYM_Minus && ! lastValue) {
			sym->type = SYM_UnaryMinus;
			symbolStackPushCopy(workStack, sym);
			lastValue = NO;
		} else if (operatorPrecedence(sym->type) != 0) {
			lastValue = NO;
			Symbol *top = symbolStackPeek(workStack);
			if (symbolStackEmpty(workStack) || top->type == SYM_Paren) {
				symbolStackPushCopy(workStack, sym);
			} else {
				while ( ! symbolStackEmpty(workStack)
					&& symbolStackPeek(workStack)->type != SYM_Paren
					&& operatorPrecedence(sym->type) >= operatorPrecedence(symbolStackPeek(workStack)->type) ) {
					top = symbolStackPop(workStack);
					symbolStackPushOriginal(exprStack, top);
				}
				symbolStackPushCopy(workStack, sym);
			}
		} else {
			// any other symbol ends expression
			break;
		}
		sym = sym->next;
	}
	while ( ! symbolStackEmpty(workStack)) {
		Symbol *top = symbolStackPop(workStack);
		symbolStackPushOriginal(exprStack, top);
	}

	// if (basicDebug) {
	// 	basicError("Work: ");
	// 	fprintSymbol(stderr, workStack, "\n");
	// 	basicError("RPN: ");
	// 	fprintSymbol(stderr, exprStack, "\n");
	// }
}

void basicRPNOpStack(Symbol *sym, Symbol *stackHead) {
	// FIXME: free all popped symbols before returning
	Symbol *a = NULL, *b = NULL;
	if (sym->type == SYM_UnaryMinus) {
		a = symbolStackEmpty(stackHead) ? NULL : symbolStackPop(stackHead);
		if ( ! a) {
			basicError("ERROR: Stack underflow for ");
			fprintSymbol(stderr, sym, "\n");
			return;
		}
	} else {
		b = symbolStackEmpty(stackHead) ? NULL : symbolStackPop(stackHead);
		a = symbolStackEmpty(stackHead) ? NULL : symbolStackPop(stackHead);
		if ( ! a || ! b) {
			basicError("ERROR: Stack underflow for ");
			fprintSymbol(stderr, sym, "\n");
			return;
		}
	}

	switch (sym->type) {
		case SYM_NEQ:
		case SYM_LTE:
		case SYM_GTE:
		case SYM_EQ:
		case SYM_LT:
		case SYM_GT: {
			int cmp = 0;
			if (a->type == SYM_Number && b->type == SYM_Number) {
				cmp = a->number - b->number;
			} else if (a->type == SYM_String && b->type == SYM_String) {
				cmp = strcmp(a->string, b->string);
			} else {
				break; // error
			}
			int res;
			switch (sym->type) {
				case SYM_NEQ: res = cmp != 0; break;
				case SYM_LTE: res = cmp <= 0; break;
				case SYM_GTE: res = cmp >= 0; break;
				case SYM_EQ: res = cmp == 0; break;
				case SYM_LT: res = cmp < 0; break;
				case SYM_GT: res = cmp > 0; break;
				default: res = 0;
			}
			symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			return;
		}
		case SYM_Plus: {
			if (a->type == SYM_Number && b->type == SYM_Number) {
				int res = a->number + b->number;
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			} else {
				char *res = stringAppend(a->type == SYM_String ? a->string : stringFromNumber(a->number),
					b->type == SYM_String ? b->string : stringFromNumber(b->number) );
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_String, 0, res));
			}
			return;
		}
		case SYM_Minus: {
			if (a->type == SYM_Number && b->type == SYM_Number) {
				int res = a->number - b->number;
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			} else {
				break; // error
			}
			return;
		}
		case SYM_Times: {
			if (a->type == SYM_Number && b->type == SYM_Number) {
				int res = a->number * b->number;
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			} else {
				break; // error
			}
			return;
		}
		case SYM_Divide: {
			if (a->type == SYM_Number && b->type == SYM_Number && b->number != 0) {
				int res = a->number / b->number;
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			} else {
				break; // error
			}
			return;
		}
		case SYM_Modulo: {
			if (a->type == SYM_Number && b->type == SYM_Number && b->number != 0) {
				int res = a->number % b->number;
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			} else {
				break; // error
			}
			return;
		}
		case SYM_UnaryMinus: {
			if (a->type == SYM_Number) {
				int res = - (a->number);
				symbolStackPushOriginal(stackHead, makeSymbol(SYM_Number, res, NULL));
			} else {
				break; // error
			}
			return;
		}
		default:
			basicError("ERROR: Unknown math operator: ");
			fprintSymbol(stderr, sym, "\n");
			return;
	}

	basicError("ERROR: Invalid operands to ");
	fprintSymbol(stderr, sym, ": ");
	fprintSymbol(stderr, a, ", ");
	fprintSymbol(stderr, b, "\n");
}

/** Advances through sym, creates an exprStack which is evaluated RPN. Returns the result from the stack. */
Symbol *basicExpr(Symbol *sym) {
	Symbol *workStack = makeSymbol(SYM_StackStart, 0, NULL);
	Symbol *exprStack = makeSymbol(SYM_StackStart, 0, NULL);

	basicExprReorder(sym, workStack, exprStack);

	// evaluate RPN
	sym = exprStack->next;
	while (sym) {
		// if (basicDebug) {
		// 	fprintf(stderr, "eval ");
		// 	fprintSymbol(stderr, sym, "\n");
		// }
		if (sym->type == SYM_Number || sym->type == SYM_String) {
			symbolStackPushCopy(workStack, sym);
		} else if (sym->type == SYM_Variable) {
			Variable *var = getVariable(sym->string);
			if (var) {
				symbolStackPushOriginal(workStack, makeSymbol(SYM_Number, var->number, var->string));
			}
		} else if (operatorPrecedence(sym->type) != 0) {
			basicRPNOpStack(sym, workStack);
		} else {
			basicError("ERROR: Unexpected symbol in expression ");
			fprintSymbol(stderr, sym, "\n");
			break;
		}
		sym = sym->next;
	}

	Symbol *res;
	if (symbolStackEmpty(workStack)) {
		basicError("ERROR: Empty stack after operation");
		res = NULL;
	} else {
		res = symbolStackPop(workStack);
	}

	freeSymbol(workStack);
	freeSymbol(exprStack);

	return res;
}

#pragma mark - Statements

int basicEnd(Symbol **symPtr) {
	#define END_SYNTAX "SYNTAX: end {EXPR}"
	Symbol *stmtSym = advanceSymbolIfTypeAndNumber(symPtr, SYM_Statement, STMT_End);
	if ( ! stmtSym) {
		basicError(END_SYNTAX ", but got ");
		fprintSymbol(stderr, *symPtr, "\n");
		return EX_USAGE;
	}
	Symbol *nextSym = *symPtr;
	if ( ! nextSym || nextSym->type == SYM_Remark) {
		exit(0);
	} else {
		Symbol *total = basicExpr(*symPtr);
		if (total->type == SYM_Number) {
			exit(total->number);
		}
	}
	basicError(END_SYNTAX ", but got ");
	fprintSymbol(stderr, *symPtr, "\n");
	return EX_USAGE;
}

int basicGoto(Symbol **symPtr) {
	#define GOTO_SYNTAX "SYNTAX: goto LINE"
	Symbol *stmtSym = advanceSymbolIfTypeAndNumber(symPtr, SYM_Statement, STMT_Goto);
	if ( ! stmtSym) {
		basicError(GOTO_SYNTAX ", but got ");
		fprintSymbol(stderr, *symPtr, "\n");
		return EX_USAGE;
	}
	Symbol *numSym = advanceSymbolIfType(symPtr, SYM_Number);
	if ( ! numSym) {
		basicError(GOTO_SYNTAX ", but got ");
		fprintSymbol(stderr, *symPtr, "\n");
		return EX_USAGE;
	}
	return gotoLineNumber(numSym->number);
}

int basicIf(Symbol **symPtr) {
	#define IF_SYNTAX "SYNTAX: if EXPR {then LINENUM} {else LINENUM}"
	Symbol *stmtSym = advanceSymbolIfTypeAndNumber(symPtr, SYM_Statement, STMT_If);
	if ( ! stmtSym) {
		basicError(IF_SYNTAX ", but got ");
		fprintSymbol(stderr, *symPtr, "\n");
		return EX_USAGE;
	}
	Symbol *total = basicExpr(*symPtr);
	BOOL ifFlag = NO;
	if (total->type == SYM_Number) {
		ifFlag = total->number != 0;
	} else {
		basicError("ERROR: Expected number, but got ");
		fprintSymbol(stderr, total, "\n");
		return EX_USAGE;
	}
	for (;;) {
		Symbol *sym = advanceSymbol(symPtr);
		Symbol *next = (sym && sym->next) ? sym->next : NULL;
		if ( ! sym || sym->type == SYM_Remark) {
			return gotoNextLineNumber();
		} else if (sym->type == SYM_Statement) {
			if (sym->number == STMT_Then && next && next->type == SYM_Number) {
				advanceSymbol(symPtr);
				if (ifFlag) {
					return gotoLineNumber(next->number);
				}
			} else if (sym->number == STMT_Else && next && next->type == SYM_Number) {
				advanceSymbol(symPtr);
				if ( ! ifFlag) {
					return gotoLineNumber(next->number);
				}
			} else {
				basicError(IF_SYNTAX ", expected then/else but got ");
				fprintSymbol(stderr, sym, "\n");
				return EX_USAGE;
			}
		} else {
			if (basicDebug) {
				fprintf(stderr, "    skip ");
				fprintSymbol(stderr, sym, "\n");
			}
		}
	}
}

int basicLet(Symbol **symPtr) {
	#define LET_SYNTAX "SYNTAX: let VAR=EXPR"
	advanceSymbolIfTypeAndNumber(symPtr, SYM_Statement, STMT_Let); // optional
	Symbol *varSym = advanceSymbolIfType(symPtr, SYM_Variable);
	if ( ! varSym) {
		basicError(LET_SYNTAX ", but expected variable: ");
		fprintSymbol(stderr, *symPtr, "\n");
		return EX_USAGE;
	}
	Variable *var = getVariable(varSym->string);
	if ( ! var) {
		return EXIT_FAILURE;
	}
	if ( ! advanceSymbolIfType(symPtr, SYM_EQ)) {
		basicError(LET_SYNTAX ", but expected '=': ");
		fprintSymbol(stderr, *symPtr, "\n");
		return EX_USAGE;
	}
	Symbol *total = basicExpr(*symPtr);
	if ( ! isStringVarname(var->name) && total->type == SYM_Number) {
		var->number = total->number;
		if (basicDebug) {
			fprintVariable(stderr, var);
		}
	} else if (isStringVarname(var->name) && total->type == SYM_String) {
		var->string = total->string;
		if (basicDebug) {
			fprintVariable(stderr, var);
		}
	} else {
		basicError("ERROR: Type mismatch ");
		fprintf(stderr, "variable '%s', value ", var->name);
		fprintSymbol(stderr, total, "\n");
		return EX_DATAERR;
	}
	return gotoNextLineNumber();
}

int basicPrint(Symbol **symPtr, FILE *fp) {
	#define PRINT_SYNTAX "SYNTAX: print|error {VALUE|;|,}..."
	BOOL newline = YES;
	Symbol *stmtSym = advanceSymbolIfType(symPtr, SYM_Statement);
	if (stmtSym && (stmtSym->number == STMT_Print || stmtSym->number == STMT_Error)) {
		// OK
	} else {
		basicError(PRINT_SYNTAX);
		fprintSymbol(stderr, stmtSym, "\n");
		return EX_USAGE;
	}
	for (;;) {
		Symbol *item = advanceSymbol(symPtr);
		if ( ! item || item->type == SYM_Remark) {
			break;
		} else if (item->type == SYM_Number) {
			newline = YES;
			fprintf(fp, "%d", item->number);
		} else if (item->type == SYM_String) {
			newline = YES;
			fprintf(fp, "%s", item->string);
		} else if (item->type == SYM_Variable) {
			newline = YES;
			Variable *var = getVariable(item->string);
			if ( ! var) {
				return EXIT_FAILURE;
			} else if (isStringVarname(var->name)) {
				fprintf(fp, "%s", var->string);
			} else {
				fprintf(fp, "%d", var->number);
			}
		} else if (item->type == SYM_Semicolon) {
			newline = NO;
		} else if (item->type == SYM_Comma) {
			newline = NO;
			fprintf(fp, "\t");
		} else {
			basicError(PRINT_SYNTAX ", but got ");
			fprintSymbol(stderr, item, "\n");
			return EX_USAGE;
		}
	}
	if (newline) {
		fprintf(fp, "\n");
	}
	fflush(fp);
	return gotoNextLineNumber();
}

int basicExec(Symbol *sym) {
	if (basicDebug) {
		fprintf(stderr, "%05d ", lineNumber);
		fprintSymbol(stderr, sym, "\n");
	}
	Symbol **symPtr = &sym;
	if (sym->type == SYM_Remark) {
		return gotoNextLineNumber();
	} else if (sym->type == SYM_Statement) {
		switch (sym->number) {
			case STMT_Let:
				return basicLet(symPtr);
			case STMT_Print:
				return basicPrint(symPtr, stdout);
			case STMT_Error:
				return basicPrint(symPtr, stderr);
			case STMT_If:
				return basicIf(symPtr);
			case STMT_Then:
			case STMT_Else: {
				basicError("SYNTAX: then/else without if");
				return EX_SOFTWARE;
			}
			case STMT_Goto:
				return basicGoto(symPtr);
			case STMT_End:
				return basicEnd(symPtr);
			default:
				basicError("ERROR: Unimplemented statement: ");
				fprintSymbol(stderr, sym, "\n");
				return EX_SOFTWARE;
		}
	} else if (sym->type == SYM_Variable) {
		return basicLet(symPtr);
	} else {
		basicError("SYNTAX: Expected statement or variable, but got: ");
		fprintSymbol(stderr, sym, "\n");
		return EX_DATAERR;
	}
}

/** Runs program, starting at line `pc`, typically 1.  Returns error code or EXIT_SUCCESS. */
int basicRun(int pc) {
	int rc = gotoLineNumber(pc);
	if (rc != EX_CONTINUE) {
		return rc;
	}
	while (lineNumber >= 1 && lineNumber < BASIC_LINENUM_MAX) {
		Symbol *sym = program[lineNumber];
		if (sym && sym->next) {
			rc = basicExec(sym->next);
			if (rc != EX_CONTINUE) {
				break;
			}
		} else {
			rc = gotoNextLineNumber();
			if (rc != EX_CONTINUE) {
				break;
			}
		}
	}
	if (rc == EX_TERMINAL) {
		rc = EXIT_SUCCESS;
	}
	return rc;
}

/** Clears out all BASIC state. */
void basicReset() {
	// if (basicDebug) {
	// 	fprintf(stderr, "reset\n");
	// }
	basicFilename = NULL;
	lineNumber = 0;
	for (int i = 0; i < BASIC_LINENUM_MAX; ++i) {
		if (program[i]) {
			freeSymbol(program[i]);
			program[i] = NULL;
		}
	}
	for (int i = 0; i < BASIC_VAR_MAX; ++i) {
		if (variable[i]) {
			freeVariable(variable[i]);
			variable[i] = NULL;
		}
	}
	linebufReset();
}

/** Displays program:linenum:message to stderr. Follow up with fprintf(stderr… for details. */
void basicError(char *message) {
	fprintf(stderr, "%s:%d: %s", basicFilename, lineNumber, message);
	fflush(stderr);
}

#pragma mark - Main

void usage() {
	fprintf(stderr, "Usage: tbasic [OPTION]... FILENAME [ARGS]...\n"
"  OPTION:\n"
"    -d, --debug 	Turn on debugging output\n"
"    -e, --exec TEXT	Execute one line\n"
"    --			All further args are passed to BASIC\n"
);
	exit(EX_USAGE);
}

int main(int argc, char **argv) {
	setlinebuf(stdout);
	basicReset();

	//printf("Hello, 1, 3='%s' Should be 'ell'\n", substring("Hello", 1, 3));
	//printf("%s\n", stringAppend("Hello ", "world!"));
	//printf("%s\n", stringFromNumber(666));

	BOOL dashdash = NO;
	char *filename = NULL;
	for (int i = 1; i < argc; ++i) {
		char *a = argv[i];
		if (dashdash || filename) {
			// FIXME: collect args for BASIC
		} else if (strncmp(a, "-", 1) == 0) {
			if (strcmp(a, "--") == 0) {
				dashdash = YES;
			} else if (strcmp(a, "-d") == 0 || strcmp(a, "--debug") == 0) {
				basicDebug = YES;
			} else if ( (strcmp(a, "-e") == 0 || strcmp(a, "--exec") == 0) && (i+1) < argc) {
				linebufReset();
				strncpy(linebuf, argv[++i], BASIC_LINE_LEN);
				int rc = parseLine();
				if (rc != EX_CONTINUE) {
					return rc;
				}
			} else {
				usage();
			}
		} else {
			filename = a;
		}
	}

	int rc = EXIT_SUCCESS;
	if (filename) {
		rc = basicLoad(filename);
	}
	if (rc == EXIT_SUCCESS || rc == EX_CONTINUE) {
		rc = basicRun(1);
	}
	if (basicDebug && rc != EXIT_SUCCESS) {
		fprintf(stderr, "Error: %d\n", rc);
	}
	return rc;
}
